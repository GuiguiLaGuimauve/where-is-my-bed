﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour {

	public void ButtonPlay () {
		Debug.Log("NEW GAME CLICKED!");
		UnityEngine.SceneManagement.SceneManager.LoadScene("_Scenes/MainScene");
	}

	public void ButtonSelectLevel () {
		Debug.Log("SELECT LEVEL CLICKED!");
		UnityEngine.SceneManagement.SceneManager.LoadScene("_Scenes/SelectLevel");
	}

	public void ButtonHelp() {
		Debug.Log("HELP CLICKED!");
		UnityEngine.SceneManagement.SceneManager.LoadScene("_Scenes/Help");
	}

	public void ButtonExit() {
		Application.Quit();
	}
}
