﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectLevelController : MonoBehaviour {

	public void ButtonLevel1 () {
		Debug.Log("LEVEL1 CLICKED!");
		UnityEngine.SceneManagement.SceneManager.LoadScene("_Scenes/MainScene");
	}

	public void ButtonLevel2 () {
		Debug.Log("LEVEL2 CLICKED!");
		UnityEngine.SceneManagement.SceneManager.LoadScene("_Scenes/Level2");
	}

	public void ButtonLevel3 () {
		Debug.Log("LEVEL3 CLICKED!");
		UnityEngine.SceneManagement.SceneManager.LoadScene("_Scenes/Level3");
	}

	public void ButtonBack() {
		Debug.Log("BACK CLICKED!");
		UnityEngine.SceneManagement.SceneManager.LoadScene("_Scenes/MainMenu");
	}
}
