﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularSawController : MonoBehaviour {

    public int speed;
    public float length;

    private float initX;
    private Vector3 pos;

    private void Start()
    {
        pos = transform.position;
        initX = pos.x;
    }

    void Update()
    {
        transform.Rotate(new Vector3(0, 45, 0) * Time.deltaTime * speed);
        float newX = initX + Mathf.Sin(Time.time * speed) * length;
        transform.position = new Vector3(newX, pos.y, pos.z);
    }
}
